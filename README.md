# Hackintosh Lenovo Ideapad 310-15IKB 80-TV
## Supported macOS versions
- macOS 10.13 High Sierra
- macOS 10.14 Mojave
- macOS 10.15 Catalina

## Specs
Motherboard: Intel Kaby Lake-U Premium PCH, Lenovo Torronto 5C2
CPU: Intel i5-7200U @ 3.1GHz (Kaby Lake)
RAM: 6GB DDR4
GPU: Intel HD Graphics 620
Display: 15.6 inch, 1920 x 1080 pixel 16:9 141 PPI, BOE NT156FHM-N31, TN LED, glossy: yes
Storage:  1 TB 5400rpm HDD
Connections: 2 USB 2.0, 1 USB 3.0 / 3.1 Gen1, 1 VGA, 1 HDMI, 1 DisplayPort, 1 Kensington Lock
Audio Connections: 1.5
Card Reader: 4-in-1 SD
Networking: Realtek RTL8168/8111 Gigabit-LAN (10/100/1000MBit), Intel Dual Band Wireless-AC 3165 (a/b/g/n/ac), Bluetooth 4.0
Optical drive: DVD +/- RW Double Layer
Battery: 30 Wh Lithium-Polymer
Camera: Webcam: 720p

## BIOS Settings
Vt-d: *Off*
Secure boot: *Off*
Intel SGX: *Off*

### Working stuff
- Audio (including headphones)
- Backlight at max
- Ethernet
- Wifi (see below)
- Bluetooth
- Microphone
- Camera
- Optical drive
- USB ports

### Not working stuff
- Battery (will be patched)
- Backlight cannot be changed

### Not tested
- Card Reader
- VGA & HDMI

## Install WiFi
1. Download Wifi installer from https://ww.lanzous.com/b01bfh3zi (pass: gejq)
2. Open config.plist and navigate to NVRAM->Add->7C436110-AB2A-4BBB-A880-FE41995C9F82 and set WiFi-SSID to your Wifi SSID and WiFi-PW to your Wifi password
3. Execute install.command
4. Reboot
